FROM nginx
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/conf.d/default.conf

docker swarm init
docker stack deploy -c docker-compose.yml swarmnodeapp
docker service ls
docker service ps
docker container ls


